﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] obstacles;
    private float delayMin = 3;
    private float delayMax = 5;
    private Vector3 bearPos = new Vector3(30, 1, 0);
    private Vector3 floatPos = new Vector3(30, 0, 0);
    private Vector3 rockPos = new Vector3(30, -2, 0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    void SpawnObstacle()
    {
        int index = Random.Range(0, 3);
        // instantiate ball at random spawn location
        //aquí va un switch y detener cunado se muere player
        Instantiate(obstacles[index], bearPos, obstacles[index].transform.rotation);
        Invoke("SpawnObstacle", Random.Range(delayMin, delayMax));
    }
}
