﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    private float scrollSpeed =8f;
    private float lBound = -3.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //detener cuando se muere player
        transform.Translate(Vector3.left * scrollSpeed * Time.deltaTime);
        if(transform.position.x < lBound)
        {
            Destroy(gameObject);
        }
    }
}
