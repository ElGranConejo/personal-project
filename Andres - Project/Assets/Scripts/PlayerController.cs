﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private int playerState = 0;
    private float playerForce = 150;
    private float returnForce = 250;
    private float bounds = 0.25f;
    //0 = normal, 1 = jump, 2 = dive, 3 = dead (3 might not be used)

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //jump command
        if (playerState == 0 && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)))
        {
            playerRb.AddForce(Vector3.up * playerForce, ForceMode.Impulse);
            playerState = 1;
            Debug.Log("Jump");
        }

        //dive command
        if (playerState == 0 && (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)))
        {
            playerRb.AddForce(Vector3.down * playerForce, ForceMode.Impulse);
            playerState = 2;
            Debug.Log("Dive");
        }

        //activate gravity
        if(transform.position.y > bounds)
        {
            playerRb.AddForce(Vector3.down * returnForce, ForceMode.Force);
        }

        //activate lift
        if (transform.position.y < -bounds)
        {
            playerRb.AddForce(Vector3.up * returnForce, ForceMode.Force);
        }

        //reset player
        switch (playerState)
        {
            case 1:
                if (transform.position.y < 0)
                {
                    playerRb.AddForce(Vector3.zero);
                    playerRb.velocity = Vector3.zero;
                    transform.position = Vector3.zero;
                    playerState = 0;
                    Debug.Log("Reset");
                }
                break;
            case 2:
                if (transform.position.y > 0)
                {
                    playerRb.AddForce(Vector3.zero);
                    playerRb.velocity = Vector3.zero;
                    transform.position = Vector3.zero;
                    playerState = 0;
                    Debug.Log("Reset");
                }
                break;
        }
    }

    //destroy the player
    public void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
